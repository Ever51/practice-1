#define _CRT_SECURE_NO_WARNINGS
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include "sorts.h"
#include "operations.h"

int getMax(int arr[], int n) {
    int mx = arr[0];
    for (int i = 1; i < n; i++)
        if (arr[i] > mx)
            mx = arr[i];
    return mx;
}

void countSort(int arr[], int n, int exp) {
    int output[100];
    int i, count[10] = { 0 };

    for (i = 0; i < n; i++)
        count[(arr[i] / exp) % 10]++;

    for (i = 1; i < 10; i++)
        count[i] += count[i - 1];

    for (i = n - 1; i >= 0; i--) {
        output[count[(arr[i] / exp) % 10] - 1] = arr[i];
        count[(arr[i] / exp) % 10]--;
    }

    for (i = 0; i < n; i++)
        arr[i] = output[i];
}

void radixsort(int arr[], int n) {
    int m = getMax(arr, n);

    for (int i = 1; m / i > 0; i *= 10)
        countSort(arr, n, i);
}

void methodOfCalculation(int n, int mass[], int sortedMass[]) {
    int k;
    for (int i = 0; i < n; i++) {
        k = 0;
        for (int j = 0; j < n; j++) {
            if (mass[i] > mass[j])
                k++;
        }
        sortedMass[k] = mass[i];
    }
}

void merge(int arr[], int l, int m, int r) {
    int i, j, k;
    int n1 = m - l + 1;
    int n2 = r - m;

    int L[100], R[100];

    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1 + j];

    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2) {
        if (L[i] <= R[j]) {
            arr[k] = L[i];
            i++;
        }
        else {
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1) {
        arr[k] = L[i];
        i++;
        k++;
    }

    while (j < n2) {
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(int arr[], int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;

        mergeSort(arr, l, m);
        mergeSort(arr, m + 1, r);

        merge(arr, l, m, r);
    }
}

void startMerge() {
    int* arr, * arr1;
    int n;
    int size, size1 = 0, i1;
    printf("������� ������ ������� ");
    scanf_s("%d", &size);
    arr = (int*)malloc(size * sizeof(int));
    arr1 = (int*)malloc(size * sizeof(int));
    printf("\n������ ������ �������� ��� � ����������(1/0)\n");
    scanf_s("%d", &n);
    if (n == 0) {
        printf("������� ������ \n");
        for (int i = 0; i <= size - 1; i++)
            scanf_s("%d", &arr[i]);
    }
    else
        randArray(arr, size);


    printf("\n��������� ������: ");
    printArray(arr, size);
    i1 = size;
    mergeSort(arr, 0, size - 1);

    while (size > 0) {
        arr1[size1] = arr[size - 1];
        size1++;
        size--;
    }
    printf("\n���������������(���� -> ���) ������: ");
    printArray(arr1, i1);

    printf("\n���������������(��� -> ����) ������: ");
    printArray(arr, i1);
    free(arr);
    free(arr1);
}
void startCounting() {
    int n;
    int size, size1 = 0, i1;
    printf("������� ������ �������: ");
    scanf_s("%d", &size);

    int* arr, * sortArr, * array1;
    arr = (int*)malloc(size * sizeof(int));
    sortArr = (int*)malloc(size * sizeof(int));
    array1 = (int*)malloc(size * sizeof(int));

    printf("������ ������ �������� ��� � ����������(1/0): \n");
    scanf_s("%d", &n);
    if (n == 0) {
        printf("��������� ������: \n");
        for (int i = 0; i < size; i++)
            scanf_s("%d", &arr[i]);
    }
    else
        randArray(arr, size);

    int j = 1;
    for (int i = 0; i < size - 1; i++) {
        if (arr[i] == arr[j]) {
            printf("������, ���� �������� �� �������� � ����������� ����������");
            exit(0);
        }
        j++;
    }

    printf("��������� ������: \n");
    printArray(arr, size);
    i1 = size;
    methodOfCalculation(size, arr, sortArr);
    while (size > 0) {
        array1[size1] = sortArr[size - 1];
        size1++;
        size--;
    }
    printf("\n���������������(���� -> ���) ������\n");
    printArray(array1, i1);

    printf("\n���������������(��� -> ����) ������:\n");
    printArray(sortArr, i1);

    printf("\n");

    free(arr);
    free(sortArr);
    free(array1);
}

void startRadix() {
    int n;
    int size, i1, size1 = 0;
    int* array, * array1;
    printf("������� ������ �������: ");
    scanf_s("%d", &size);
    array = (int*)malloc(size * sizeof(int));
    array1 = (int*)malloc(size * sizeof(int));
    i1 = size;

    printf("������ ������ �������� ��� � ����������(1/0):  \n");
    scanf_s("%d", &n);
    if (n == 0) {
        printf("��������� ������ \n");
        for (int i = 0; i < size; i++) {
            scanf_s("%d", &array[i]);
            if (array[i] < 0) {
                printf("������, ���� �������� �� �������� � �������������� ����������");
                exit(0);
            }
        }
    }
    else
        randArray(array, size);

    printf("��������� ������: \n");
    printArray(array, i1);

    radixsort(array, size);
    while (size > 0) {
        array1[size1] = array[size - 1];
        size1++;
        size--;
    }
    printf("\n���������������(���� -> ���) ������\n");
    printArray(array1, i1);

    printf("\n���������������(��� -> ����) ������:\n");
    printArray(array, i1);
    free(array);
    free(array1);
}