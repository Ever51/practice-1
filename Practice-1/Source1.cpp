#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "operations.h"

void printArray(int arr[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

void randArray(int arr[], int size) {
    srand(time(NULL));
    for (int i = 0; i < size; i++)
        arr[i] = rand() % 100 + 1;
}