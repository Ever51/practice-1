//sorts.h
#ifndef SORTS_H
#define SORTS_H
void startMerge();
void startCounting();
void startRadix();
#endif //SORTS_H